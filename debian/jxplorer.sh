#!/bin/sh
#
#
BASEPATH=/usr/share/jxplorer

. /usr/lib/java-wrappers/java-wrappers.sh

find_java_runtime java6

find_jars jhall junit
find_jars /usr/share/jxplorer/jxplorer.jar
find_jars /usr/share/jxplorer/jxplorer_help.jar

JAVA_ARGS="-Djxplorer.config=user.home"

cd $BASEPATH
run_java com.ca.directory.jxplorer.JXplorer "$@"
