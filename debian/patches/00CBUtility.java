Description: User specific settings in ~/.jxplorer
Author: Gabriele Giacone <1o5g4r8o@gmail.com>

--- a/src/com/ca/commons/cbutil/CBUtility.java
+++ b/src/com/ca/commons/cbutil/CBUtility.java
@@ -1415,7 +1415,7 @@ public class CBUtility
         }
 
         // try default 'user home' location...
-        defaultConfigDirectory = System.getProperty("user.home") + File.separator + applicationName;
+        defaultConfigDirectory = System.getProperty("user.home") + File.separator + "." + applicationName + File.separator;
         if (checkAndCreateWorkingDirectory(defaultConfigDirectory))
             return defaultConfigDirectory;
 
